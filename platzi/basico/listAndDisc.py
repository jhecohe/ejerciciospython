def run():
    unaLsita = [1, "Hello", True, 4.5]
    myDic = {"firstName":"jherson", "lastName":"Cohecha"}
    superList = [
        {"firstName":"jherson", "lastName":"Cohecha"},
        {"firstName":"Maria", "lastName":"Fernandez"},
        {"firstName":"Matthew", "lastName":"Cohecha"},
    ]
    superDict = {
        "naturalNumbers":[1,2,3,4,5],
        "integerNumbers":[-1,-2,0,1,2],
        "floatNumbers": [4.1,3.2,6.43]
    }

    for key, value in superDict.items():
        print(key, "-", value)

    print("*******************************************")

    for value in superList:
        for key, valor in value.items():
            print(key, "-", valor)

if __name__ == '__main__':
    run()