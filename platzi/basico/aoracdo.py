from random import randint
import os

def cargarDiccionario():
    diccionary = []
    with open("files/palabras.txt", "r", encoding="utf-8") as d:
        for word in d:
            diccionary.append(word)

    return diccionary

def capturarLetra():
    letter = input("Intruduce a letter: ")
    print(letter)
    return letter

def obtenerPalabra():
    dic = cargarDiccionario()
    num = randint(0, len(dic))
    selectedWord = dic[num]
    return selectedWord

def mostrarPantalla(respuesta):
    os.system("clear")
    adivinado = "".join(respuesta[0])
    titulo = "============Juego del aoracado=========\n"
    siConcide = "========= Excelente ===========\n"
    noConcide = "========= Try Again ===========\n"
    if(respuesta[1]):
        pantalla = titulo+siConcide+adivinado
    else:
        pantalla = titulo+noConcide+adivinado
    print(pantalla)

def validarLetra(letra, palabra, adivinado):
    pal = []
    pal [:0]= adivinado
    cadena=[]
    cadena [:0]= palabra
    concidir = False
    for i in range(0, len(cadena)):
        if(cadena[i] == letra):
            pal[i]=cadena[i]
            concidir = True
        elif(pal[i]!="_"):
            pal[i]=pal[i]

    respuesta = [pal, concidir]
    return respuesta

def cargarAdivinado(palabra):
    cadena = []
    cadena [:0]= palabra
    adivinado = ""
    for guion in cadena:
        adivinado = adivinado + "_"
    return adivinado

def estadoAorcado(adivinado):
    estado = True
    for carac in adivinado:
        if carac == "_":
            estado = False
            break
    return estado

def run():
    palabra = "matthew"#obtenerPalabra()
    contador = 0
    adivinado = cargarAdivinado(palabra)
    estado = False
    while contador<len(palabra)+2:
        letra = capturarLetra()
        respuesta = validarLetra(letra, palabra, adivinado)
        mostrarPantalla(respuesta)
        estado = estadoAorcado(respuesta[0])
        if estado:
            break
        adivinado = "".join(respuesta[0])
        contador+=1
        
    input("!!!!Gracias por jugar!!!! \n Oprime Enter para salir")

if __name__=="__main__":
    os.system("clear")
    run()