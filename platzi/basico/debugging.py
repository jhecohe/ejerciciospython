def divisores(num):
    numeros = [i for i in range(1, num+1) if num % i == 0]
    return numeros


def run():
    try:
        valor = int(input("Ingrese un numero: "))
        if(valor < 0):
            raise numNegativos()
        print(divisores(valor))
    except ValueError:
        print("Solo puede ingresar numeros: ")
    except Exception as numNegativos:
        print("No se puede trabajar con numeros negativos: ")


if __name__ == "__main__":
    run()
