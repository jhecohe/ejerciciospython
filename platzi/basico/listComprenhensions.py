def run():
    # square = []
    # for i  in range(1, 101):
    #     cuadrado = i**2
    #     if cuadrado % 3 == 0:
    #         square.append(cuadrado)

    square = [i**2 for i in range(1, 101) if i%3==0]
    
    print(square)


    multiplos469 = [i for i in range(1, 1001) if i%6==0 and i%9==0 and i%4==0]

    print("----------------------Multiplos--------------------------")
    print(multiplos469)

if __name__ == "__main__":
    run()