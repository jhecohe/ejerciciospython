def read():
    numbers = []
    with open("files/numbers.txt", "r", encoding="utf-8") as f:
        for line in f:
            numbers.append(int(line))
        print(numbers)

def write():
    nombres = ["matthew", "maria", "jherson", "camilo", "margaret", "alex", "mario"]
    with open("files/nombres.txt", "w", encoding="utf-8") as w: #also we could use "a" to append on the text
        for nombre in nombres:
            w.write(nombre)

def run():
    #read()
    write()


if __name__ == "__main__":
    run()
