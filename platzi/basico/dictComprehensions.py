from math import sqrt

def run():
    dic = {}
    for i in range(1, 100):
        if i%3 != 0:
            dic[i]=i**3

    print(dic)
    print("_______________comprehension________________")
    # diccionario = {i:i**3 for i in range(1, 100) if i%3!=0}
    # print(diccionario)

    print("______________Las primeras 1000 raices cuadradas__________________")

    raices = {i:sqrt(i) for i in range(1,101)}
    
    print(raices)

if __name__ == "__main__":
    run()