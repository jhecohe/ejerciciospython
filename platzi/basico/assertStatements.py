def divisores(num):
    numeros = [i for i in range(1, num+1) if num % i == 0]
    return numeros


def run():
    valor = input("Ingrese un numero: ")
    assert valor.isnumeric(), "Debes ingresar un numero"
    print(divisores(int(valor)))


if __name__ == "__main__":
    run()
