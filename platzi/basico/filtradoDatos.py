DATA = [
    {
        'name': 'Facundo',
        'age': 72,
        'organization': 'Platzi',
        'position': 'Technical Coach',
        'language': 'python',
    },
    {
        'name': 'Luisana',
        'age': 33,
        'organization': 'Globant',
        'position': 'UX Designer',
        'language': 'javascript',
    },
    {
        'name': 'Héctor',
        'age': 19,
        'organization': 'Platzi',
        'position': 'Associate',
        'language': 'ruby',
    },
    {
        'name': 'Gabriel',
        'age': 20,
        'organization': 'Platzi',
        'position': 'Associate',
        'language': 'javascript',
    },
    {
        'name': 'Isabella',
        'age': 30,
        'organization': 'Platzi',
        'position': 'QA Manager',
        'language': 'java',
    },
    {
        'name': 'Karo',
        'age': 23,
        'organization': 'Everis',
        'position': 'Backend Developer',
        'language': 'python',
    },
    {
        'name': 'Ariel',
        'age': 32,
        'organization': 'Rappi',
        'position': 'Support',
        'language': '',
    },
    {
        'name': 'Juan',
        'age': 17,
        'organization': '',
        'position': 'Student',
        'language': 'go',
    },
    {
        'name': 'Pablo',
        'age': 32,
        'organization': 'Master',
        'position': 'Human Resources Manager',
        'language': 'python',
    },
    {
        'name': 'Lorena',
        'age': 56,
        'organization': 'Python Organization',
        'position': 'Language Maker',
        'language': 'python',
    },
]


def run():
    #filtroLenguage("java")
    # filtroTrabajo("Platzi")
    #filtroEdad(18)
    #filtroViejo()
    filtroViejoComprenhensions()

def filtroLenguage(language):
    workers = [worker["name"] for worker in DATA if worker["language"]==language]
    for worker in workers:
        print(worker)

def filtroTrabajo(organization):
    workers = [worker["name"] for worker in DATA if worker["organization"]==organization]
    for worker in workers:
        print(worker)

def filtroEdad(edad):
    workers = list(filter(lambda worker:worker["age"]>17, DATA))
    workers = list(map(lambda worker: worker["name"] , workers))
    for worker in workers:
        print(worker)

def filtroViejo():
    workers = list(map(lambda worker:worker|{"old":worker["age"]>70}, DATA)) #Desde la version 3.9
    # workers = list(map(lambda worker:{**worker, **{"old":worker["age"]>70}}, DATA)) versiones ppython 3.5 a 3.8
    for worker in workers:
        print(worker)

def filtroViejoComprenhensions():
    workers = [worker|{"old":worker["age"]>70} for worker in DATA]
    for worker in workers:
        print(worker)

if __name__=="__main__":
    run()
