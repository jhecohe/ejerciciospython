
from functools import reduce

def filtrar():
    lista = [1,2,34,56,78,245,567,57,6,3]
    pares = list(filter(lambda x: x%2 == 0, lista))
    print("__________________pares______________")
    print(pares)


def mapas():
    lista =  [1,2,3,4,5]
    cuadrados = [i**2 for i in lista]
    # print(cuadrados)

    squares = list(map(lambda x: x**2, lista))
    print("__________________cuadrados__________________")
    print(squares)

def reducir(): # Se reduce la lista a un solo valor toca importar functools
    lista= [2,2,2,2,2]
    reducido = reduce(lambda x, y: x*y, lista)
    print(reducido)

if __name__=="__main__":
    #filtrar()
    #mapas()
    reducir()
