#La busqueda binaria consiste en tener un objetivo ordenado he ir reduciendo el objetivo a la mitad
# En este caso tenemos que buscar la raiz cuadrada de un numero donde definimos un valor epsilon
# que va a ser el acercamiento maximo en caso de que la raiz no sea exacta
#  
objetivo = int(input('Digita un numero: '))
epsilon = 0.01
bajo = 0.0
alto = max(1.0, objetivo)
print(f'Max valor {max(1.0, objetivo)}')
respuesta = (alto + bajo) / 2

while abs(respuesta**2 - objetivo) >= epsilon:
    print(f'Alto => {alto}, Bajo => {bajo}, Respuesta => {respuesta}, Culculo Abs => {abs(respuesta**2 - objetivo)}')
    if respuesta**2 < objetivo:
        bajo = respuesta
    else:
        alto = respuesta

    respuesta = (alto + bajo) / 2
    print(f' Epsilon mayo => {abs(respuesta**2 - objetivo) >= epsilon}')
print(f'La raiz cuadrada {objetivo} es igual a {respuesta}')