
def fibonacci(numero):
    if numero == 0 or numero == 1:
        return 1

    return fibonacci(numero - 1) + fibonacci(numero - 2)

def main():
    numero = int(input('Ingrese un numero: '))
    print(fibonacci(numero))

if __name__ == '__main__':
    main()