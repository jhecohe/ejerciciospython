
tupla = ()
tupla = (1, 'Tupla', True)
print(tupla[0]) #1

tuplaConUnSoloValor = (1,)
print(tuplaConUnSoloValor)

tuplaNumeros = (1,2,3,4,5,6,7)
z,x,c,v,b,n,m = tuplaNumeros
print(f'{z}{x}{c}{v}{b}')

def cordenadas():
    x = 5
    y = 7
    return (x,y)

q, w=cordenadas()
print(f'{q}{w}')